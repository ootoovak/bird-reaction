#![feature(copysign)]
use std::f64::consts::PI;
use std::iter::Sum;
use std::ops::Sub;

const ATTRACTIVE_CONSTANT: f64 = 0.1;
const MAXIMUM_ACCELERATION: f64 = 0.0625;

#[derive(Debug)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[derive(Debug)]
pub struct Distance {
    pub magnitude: f64,
    pub angle: f64,
}

impl Sub for &Point {
    type Output = Point;

    fn sub(self, other: &Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a> Sum<&'a Self> for Point {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = &'a Self>,
    {
        iter.fold(Self { x: 0.0, y: 0.0 }, |a, b| Self {
            x: a.x + b.x,
            y: a.y + b.y,
        })
    }
}

impl Point {
    pub fn to_distance(&self, other: &Point) -> Distance {
        Distance {
            magnitude: self.magnitude(other),
            angle: self.angle(other),
        }
    }

    fn magnitude(&self, other: &Point) -> f64 {
        let relative_point = other - self;
        (relative_point.x.powi(2) + relative_point.y.powi(2)).sqrt()
    }

    fn angle(&self, other: &Point) -> f64 {
        let relative_point = other - self;
        let angle = relative_point.y.atan2(relative_point.x);
        if angle < 0.0 {
            (2.0 * PI) + angle
        } else {
            angle
        }
    }
}

impl Distance {
    pub fn to_point(&self) -> Point {
        Point {
            x: self.x(),
            y: self.y(),
        }
    }

    fn x(&self) -> f64 {
        self.magnitude * self.angle.cos()
    }
    fn y(&self) -> f64 {
        self.magnitude * self.angle.sin()
    }
}

pub fn calculate_attraction(magnitude: f64) -> f64 {
    let min_distance = 0.000_000_001;

    let distance = if magnitude <= min_distance {
        -1.0 * min_distance
    } else {
        magnitude
    };

    let result = ATTRACTIVE_CONSTANT / distance.powi(3);

    if result.abs() > MAXIMUM_ACCELERATION {
        -1.0 * result
    } else {
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calculates_an_attractive_force() {
        let magnitude = 4.472_135_954_999_58;
        let result = calculate_attraction(magnitude);
        assert!(result - 0.049_999_999_999_999_99 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_distance_between_points() {
        let point1 = Point { x: 0.0, y: 0.0 };
        let point2 = Point { x: 4.0, y: 2.0 };

        assert!(point1.magnitude(&point2) - 4.472_135_954_999_58 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_distance_between_points_with_positive_offset_from_origin() {
        let point1 = Point { x: 2.0, y: 3.0 };
        let point2 = Point { x: 6.0, y: 5.0 };

        assert!(point1.magnitude(&point2) - 4.472_135_954_999_58 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_distance_between_points_with_negative_offset_from_origin() {
        let point1 = Point { x: -2.0, y: -3.0 };
        let point2 = Point { x: 2.0, y: -1.0 };

        assert!(point1.magnitude(&point2) - 4.472_135_954_999_58 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_angle_between_points_in_the_first_quadrant() {
        let point1 = Point { x: 0.0, y: 0.0 };
        let point2 = Point { x: 3.0, y: 4.0 };

        assert!(point1.angle(&point2) - 0.927_295_218_001_612_2 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_angle_between_points_in_the_second_quadrant() {
        let point1 = Point { x: 0.0, y: 0.0 };
        let point2 = Point { x: -3.0, y: 4.0 };

        assert!(point1.angle(&point2) - 2.214_297_435_588_181 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_angle_between_points_in_the_third_quadrant() {
        let point1 = Point { x: 0.0, y: 0.0 };
        let point2 = Point { x: -3.0, y: -4.0 };

        assert!(point1.angle(&point2) - 4.068_887_871_591_405 < std::f64::EPSILON);
    }

    #[test]
    fn can_calculate_the_angle_between_points_in_the_fourth_quadrant() {
        let point1 = Point { x: 0.0, y: 0.0 };
        let point2 = Point { x: 3.0, y: -4.0 };

        assert!(point1.angle(&point2) - 5.355_890_089_177_974 < std::f64::EPSILON);
    }
}
