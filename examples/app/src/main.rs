extern crate quicksilver;
extern crate bird_reaction;
extern crate uuid;

use bird_reaction::{Point, Distance, calculate_attraction};
use uuid::Uuid;
use rand::Rng;

use quicksilver::{
    geom::{Circle, Transform, Vector},
    graphics::{Background::Col, Color},
    lifecycle::{run, Settings, State, Window},
    Result,
};

const WINDOW_WIDTH: i32 = 1440;
const WINDOW_HEIGHT: i32 = 810;

const WINDOW_WIDTH_ZERO: i32 = WINDOW_WIDTH / 2;
const WINDOW_HEIGHT_ZERO: i32 = WINDOW_HEIGHT / 2;

const MIN_X: f64 = -1.0 * WINDOW_WIDTH_ZERO as f64 + BIRD_RADIUS;
const MAX_X: f64 = WINDOW_WIDTH_ZERO as f64 - BIRD_RADIUS;
const MIN_Y: f64 = -1.0 * WINDOW_HEIGHT_ZERO as f64 + BIRD_RADIUS;
const MAX_Y: f64 = WINDOW_HEIGHT_ZERO as f64 - BIRD_RADIUS;

const BIRD_RADIUS: f64 = 10.0;
const LOCAL_AREA_RADIUS: f64 = 100.0;

#[derive(Debug)]
struct Bird {
    id: Uuid,
    local_area_radius: f64,
    position: Point,
    velocity: Point,
    acceleration: Point
}

impl Bird {
    pub fn new(position: Point, velocity: Point, acceleration: Point) -> Bird {
        Bird {
            id: Uuid::new_v4(),
            local_area_radius: LOCAL_AREA_RADIUS,
            position,
            velocity,
            acceleration
        }
    }

    pub fn rand() -> Bird {
        let mut rng = rand::thread_rng();
        Bird::new(
            Point { x: rng.gen_range(-0.0625, 0.0625), y: rng.gen_range(-0.0625, 0.0625) },
            Point { x: rng.gen_range(-1.0, 1.0), y: rng.gen_range(-1.0, 1.0) },
            Point { x: rng.gen_range(MIN_X, MAX_X), y: rng.gen_range(MIN_Y, MAX_Y) }
        )
    }

    pub fn in_local_area(&self, other_bird: &Bird) -> bool {
        let distance_to_other_bird = self.position.to_distance(&other_bird.position).magnitude;
        distance_to_other_bird <= self.local_area_radius
    }
}

#[derive(Debug)]
struct Bounds {
    max: Point,
    min: Point
}

impl Bounds {
    pub fn new(origin: &Point, max_distance: f64) -> Bounds {
        Bounds {
            max: Point {
                x: origin.x + max_distance,
                y: origin.y + max_distance
            },
            min: Point {
                x: origin.x - max_distance,
                y: origin.y - max_distance,
            }
        }
    }

    pub fn point_inside(&self, point: &Point) -> bool {
        (point.y <= self.max.y) &&
        (point.y >= self.min.y) &&
        (point.x <= self.max.x) &&
        (point.x >= self.min.x)
    }
}

#[derive(Debug)]
struct World {
    birds: Vec<Bird>
}

fn calculate_new_acceleration(point: &Point, points: &[&Point]) -> Point {
    let new_points: Vec<Point> = points
        .iter()
        .map(|other_point| point.to_distance(other_point))
        .map(|distance| Distance {
            magnitude: calculate_attraction(distance.magnitude),
            ..distance
        })
        .map(|distance| distance.to_point())
        .collect();

    let new_point: Point = new_points.iter().sum();

    new_point
}

fn calculate_new_velocity(velocity: &Point, acceleration: &Point) -> Point {
    Point {
        x: velocity.x + (acceleration.x),
        y: velocity.y + (acceleration.y)
    }
}

fn calculate_new_position(position: &Point, velocity: &Point) -> Point {
    let mut x = position.x + (velocity.x);
    let mut y = position.y + (velocity.y);

    if x < -1.0 * WINDOW_WIDTH_ZERO as f64 + BIRD_RADIUS {
        x = -1.0 * WINDOW_WIDTH_ZERO as f64 + BIRD_RADIUS;
    }

    if x > WINDOW_WIDTH_ZERO as f64 - BIRD_RADIUS {
        x = WINDOW_WIDTH_ZERO as f64 - BIRD_RADIUS;
    }

    if y < -1.0 * WINDOW_HEIGHT_ZERO as f64 + BIRD_RADIUS {
        y = -1.0 * WINDOW_HEIGHT_ZERO as f64 + BIRD_RADIUS;
    }

    if y > WINDOW_HEIGHT_ZERO as f64 - BIRD_RADIUS {
        y = WINDOW_HEIGHT_ZERO as f64 - BIRD_RADIUS;
    }

    Point { x, y }
}

impl State for World {
    fn new() -> Result<World> {
        let number_of_birds = 50;

        let mut birds = Vec::with_capacity(number_of_birds);
        for _times in 0..number_of_birds {
            birds.push(Bird::rand());
        }

        Ok(World { birds })
    }

    fn update(&mut self, _window: &mut Window) -> Result<()> {
        self.birds = self.birds
            .iter()
            .map(|bird| {
                let local_bounds = Bounds::new(&bird.position, bird.local_area_radius);

                let local_bird_positions: Vec<&Point> = self.birds
                    .iter()
                    .filter(|other_bird| local_bounds.point_inside(&other_bird.position))
                    .filter(|other_bird| bird.in_local_area(other_bird))
                    .map(|other_bird| &other_bird.acceleration)
                    .collect();

                let acceleration = calculate_new_acceleration(&bird.position, local_bird_positions.as_slice());
                let velocity = calculate_new_velocity(&bird.velocity, &acceleration);
                let position = calculate_new_position(&bird.position, &velocity);

                println!("Bird:\n{:?}\n{:#?}\n{:#?}\n{:#?}", bird.id, bird.acceleration, bird.velocity, bird.position);

                Bird { acceleration, velocity, position, ..*bird }
            })
            .collect();

        println!("=========================");

        Ok(())
    }

    fn draw(&mut self, window: &mut Window) -> Result<()> {
        window.clear(Color::BLACK)?;

        for bird in self.birds.iter() {
            let x: f32 = bird.position.x as f32;
            let y: f32 = -1.0 * bird.position.y as f32;
            let size = BIRD_RADIUS as f32;
            let local_area_size = bird.local_area_radius as f32;

            window.draw_ex(
                &Circle::new((x, y), local_area_size),
                Col(Color::RED.with_alpha(0.25)),
                Transform::translate((WINDOW_WIDTH_ZERO, WINDOW_HEIGHT_ZERO)),
                1,
            );

            window.draw_ex(
                &Circle::new((x, y), size),
                Col(Color::GREEN.with_alpha(0.5)),
                Transform::translate((WINDOW_WIDTH_ZERO, WINDOW_HEIGHT_ZERO)),
                2,
            );
        }

        Ok(())
    }
}

fn main() {
    run::<World>(
        "Bird Reaction",
        Vector::new(WINDOW_WIDTH, WINDOW_HEIGHT),
        Settings::default()
    );
}
